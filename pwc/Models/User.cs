﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace pwc.Models
{
    public class User
    {
        public int UserID { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        public string UserName { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "This field is required.")]
        public string UserPassword { get; set; }

        public virtual ICollection<Case> Cases { get; set; }

        [NotMapped]
        public string LoginErrorMessage { get; set; }
    }
}