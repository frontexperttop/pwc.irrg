﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pwc.Models
{
    public class AnnexureB
    {
        public int AnnexureBID { get; set; }
        public string B_Description { get; set; }
        public string B_Reason { get; set; }

        public Nullable<int> CaseID { get; set; }
        public virtual Case Case { get; set; }
    }
}