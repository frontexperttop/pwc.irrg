﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pwc.Models
{
    public class AnnexureC
    {
        public int AnnexureCID { get; set; }
        public string C_Description { get; set; }
        public string C_Involved { get; set; }
        public string C_Reason { get; set; }

        public Nullable<int> CaseID { get; set; }
        public virtual Case Case { get; set; }
    }
}