﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace pwc.Models
{
    public class Case
    {
        public int CaseID { get; set; }
        //Phase 1: Determining Irregular Expenditure
        //Open A Case
        public string CaseNumber { get; set; }
        public string OrderNumber { get; set; }
        public string ContactRefferenceNumber { get; set; }
        public Nullable<decimal> ContactValueIncVAT { get; set; }
        public string PayeeOrBenificiery { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Description { get; set; }
        public Nullable<int> CategoryID { get; set; }
        [NotMapped]
        public string NewCategory { get; set; }
        //Unsolicited Bids
        public string Feasibility { get; set; }
        public string InnovativeDesign { get; set; }
        public string InnovativeApproach { get; set; }
        public string EffectiveMethod { get; set; }
        public string OtherSuppliers { get; set; }
        public string UnsolicitedBidConclusion { get; set; }
        //Payments
        public string OneQuoteAttached { get; set; }
        public string OneQuoteAttachedRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase OneQuoteAttachedFile { get; set; }
        public string ThreeQuotesAttached { get; set; }
        public string ThreeQuotesAttachedRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase ThreeQuotesAttachedFile { get; set; }
        public string RelateToContract { get; set; }
        public string RelateToContractRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase RelateToContractFile { get; set; }
        public string TransversalContract { get; set; }
        public string TransversalContractRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase TransversalContractFile { get; set; }
        public string Deviation { get; set; }
        public string DeviationRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase DeviationFile { get; set; }
        public string WinningSupplier { get; set; }
        public string WinningSupplierRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase WinningSupplierFile { get; set; }
        public string PaymentConclusion { get; set; }

        //Phase 2: Determining whether there was a loss
        //Part A: Determining the loss amount
        public string LowestQuote { get; set; }
        public string LowestQuoteRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase LowestQuoteFile { get; set; }
        public string ApprovedDeviation { get; set; }
        public string ApprovedDeviationRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase ApprovedDeviationFile { get; set; }
        public string AdequatelyDocumentedDeviation { get; set; }
        public string AdequatelyDocumentedDeviationRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase AdequatelyDocumentedDeviationFile { get; set; }
        public Nullable<decimal> CalculatedLoss { get; set; }
        public string AdditionalInformation { get; set; }
        //Part B: Confirming receipt of goods and services  (in all instances)
        public string GoodsWereRendered { get; set; }
        public string GoodsWereRenderedRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase GoodsWereRenderedFile { get; set; }
        public string AssetRegister { get; set; }
        public string AssetRegisterRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase AssetRegisterFile { get; set; }
        //Part C: Employee Related Payments (only in instances of a loss and where a payment is made to an official(s))
        public string DefinitionOfExpenditure { get; set; }
        public string DefinitionOfExpenditureRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase DefinitionOfExpenditureFile { get; set; }
        public string RelatedPaymentsLoss { get; set; }
        public string RelatedPaymentsLossRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase RelatedPaymentsLossFile { get; set; }
        public string PaymentMade { get; set; }
        public string PaymentMadeRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase PaymentMadeFile { get; set; }
        //Part D: Conclusion
        public string Loss { get; set; }
        
        //Phase 3 : Determining whether an official is liable in law
        public string OfficialDetermining { get; set; }
        public string OfficialDeterminingRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase OfficialDeterminingFile { get; set; }
        public Nullable<int> OfficialNumber { get; set; }
        public string OfficialName { get; set; }
        public string Possition { get; set; }
        public string Role { get; set; }
        public string ExceedPowers { get; set; }
        public string ExceedPowersRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase ExceedPowersFile { get; set; }
        public string UseOfAlcohol { get; set; }
        public string UseOfAlcoholRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase UseOfAlcoholFile { get; set; }
        public string ActInScope { get; set; }
        public string ActInScopeRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase ActInScopeFile { get; set; }
        public string ActRectlessly { get; set; }
        public string ActRectlesslyRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase ActRectlesslyFile { get; set; }
        public string WithoutPriorConsultation { get; set; }
        public string WithoutPriorConsultationRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase WithoutPriorConsultationFile { get; set; }
        public string OfficialFail { get; set; }
        public string OfficialFailRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase OfficialFailFile { get; set; }
        public string OfficialConclusion { get; set; }

        //Phase 4.1: Disciplinary action
        public string MeetIrregularExpenditure { get; set; }
        public string MeetIrregularExpenditureRemarks { get; set; }
        [NotMapped]
        public HttpPostedFileBase MeetIrregularExpenditureFile { get; set; }

        //Phase 4.2: Condonation
        public string CondoningAuthority { get; set; }
        public Nullable<decimal> TotalRandValue { get; set; }

        public int Status { get; set; } = 0;

        [Required]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        public virtual ICollection<CaseFile> CaseFiles { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
        public virtual ICollection<AnnexureB> AnnexureB { get; set; }
        public virtual ICollection<AnnexureC> AnnexureC { get; set; }
        public virtual ICollection<AnnexureD> AnnexureD { get; set; }
        public virtual ICollection<AnnexureF> AnnexureF { get; set; }
        public virtual ICollection<AnnexureG> AnnexureG { get; set; }

        [NotMapped]
        public SelectList CategoryList { get; set; }
    }
}