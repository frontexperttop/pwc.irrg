﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pwc.Models
{
    public class CaseFile
    {
        public int CaseFileID { get; set; }
        public string FieldName { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }

        public Nullable<int> CaseID { get; set; }
        public virtual Case Case { get; set; }
    }
}