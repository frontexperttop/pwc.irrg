﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pwc.Models
{
    public class AnnexureG
    {
        public int AnnexureGID { get; set; }
        public string EvidenceIndicating { get; set; }
        public string TransgressionReason { get; set; }
        public string DetailedMotivation { get; set; }
        public string DetailOfTransgression { get; set; }
        public string ReferenceToRelevantLegislation { get; set; }
        public string DeviatingReason { get; set; }
        public string IndicateStateLoss { get; set; }
        public string FromOneBidder { get; set; }
        public string FinancialImplications { get; set; }
        public string Contractors { get; set; }
        public string CorrectiveSteps { get; set; }
        public string RelevantSupportingDocumentation { get; set; }

        public Nullable<int> CaseID { get; set; }
        public virtual Case Case { get; set; }
    }
}