﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pwc.Models
{
    public class AnnexureF
    {
        public int AnnexureFID { get; set; }
        public Nullable<DateTime> F_DateOfFirstPayment { get; set; }
        public Nullable<DateTime> F_DateOfLastPayment { get; set; }
        public string F_Description { get; set; }
        public string F_OfficialName { get; set; }
        public string F_Position { get; set; }
        public string F_Role { get; set; }
        public string F_InstructionNotCompliedWith { get; set; }
        public Nullable<decimal> F_LossAmount { get; set; }

        public Nullable<int> CaseID { get; set; }
        public virtual Case Case { get; set; }
    }
}