﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace pwc.Models
{
    [NotMapped]
    public class Summary
    {
        public int TotalIRR { get; set; } = 0;
        public int ResolvedIRR { get; set; } = 0;
        public int UnresolvedIRR { get; set; } = 0;
        public int PendingHearing { get; set; } = 0;
        public int LossIRR { get; set; } = 0;
        public int OfficialsInLaw { get; set; } = 0;
        public int UnsolicitedBids { get; set; } = 0;
        public int R10K { get; set; } = 0;
        public int R30K { get; set; } = 0;
        public int R500K { get; set; } = 0;
        public int AboveR500K { get; set; } = 0;
        public ICollection<Case> LastIRRs { get; set; }
        
    }
}