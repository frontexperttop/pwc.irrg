﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pwc.Models
{
    public class Category
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }

        public virtual ICollection<Case> Cases { get; set; }
    }
}