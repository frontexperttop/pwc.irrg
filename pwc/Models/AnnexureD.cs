﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pwc.Models
{
    public class AnnexureD
    {
        public int AnnexureDID { get; set; }
        public string D_Description { get; set; }
        public string D_NamesOfOfficial { get; set; }
        public string D_Reason { get; set; }

        public Nullable<int> CaseID { get; set; }
        public virtual Case Case { get; set; }
    }
}