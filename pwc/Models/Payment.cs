﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Web;

namespace pwc.Models
{
    public class Payment
    {
        public int PaymentID { get; set; }
        public string PaymentNumber { get; set; }
        public string OrderNumber { get; set; }
        public DateTime PaymentDate { get; set; }
        public Nullable<decimal> PaymentAmount { get; set; }

        public Nullable<int> CaseID { get; set; }
        public virtual Case Case { get; set; }

        [NotMapped]
        public string DateString { get; set; }

        public string PaymentDateString()
        {
            CultureInfo gb = new CultureInfo("en-GB");
            string result = PaymentDate.ToString("dd/MM/yyyy", gb);
            return result;
        }
    }
}