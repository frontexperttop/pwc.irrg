﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pwc.Models;

namespace pwc.Repository
{
    public class AnnexureCRepository
    {
        private PWCContext database { get; set; }
        public AnnexureCRepository(PWCContext context)
        {
            this.database = context;
        }
        public AnnexureC GetByCaseID(int CaseID)
        {
            var result = database.AnnexureCs.AsQueryable();
            result = result.Where(x => x.CaseID == CaseID);
            if (result.Count() > 0)
                return result.First();
            else
                return null;
        }
        public void Save(AnnexureC AC)
        {
            var tempData = GetByCaseID(AC.CaseID ?? 0);
            if (tempData == null)
            {
                database.AnnexureCs.Add(AC);
            }
            else
            {
                tempData.C_Description = AC.C_Description;
                tempData.C_Involved = AC.C_Involved;
                tempData.C_Reason = AC.C_Reason;
            }
            database.SaveChanges();
        }
    }
}