﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pwc.Models;

namespace pwc.Repository
{
    public class AnnexureGRepository
    {
        private PWCContext database { get; set; }
        public AnnexureGRepository(PWCContext context)
        {
            this.database = context;
        }
        public AnnexureG GetByCaseID(int CaseID)
        {
            var result = database.AnnexureGs.AsQueryable();
            result = result.Where(x => x.CaseID == CaseID);
            if (result.Count() > 0)
                return result.First();
            else
                return null;
        }
        public void Save(AnnexureG AG)
        {
            var tempData = GetByCaseID(AG.CaseID ?? 0);
            if (tempData == null)
            {
                database.AnnexureGs.Add(AG);
            }
            else
            {
                tempData.EvidenceIndicating = AG.EvidenceIndicating;
                tempData.TransgressionReason = AG.TransgressionReason;
                tempData.DetailedMotivation = AG.DetailedMotivation;
                tempData.DetailOfTransgression = AG.DetailOfTransgression;
                tempData.ReferenceToRelevantLegislation = AG.ReferenceToRelevantLegislation;
                tempData.DeviatingReason = AG.DeviatingReason;
                tempData.IndicateStateLoss = AG.IndicateStateLoss;
                tempData.FromOneBidder = AG.FromOneBidder;
                tempData.FinancialImplications = AG.FinancialImplications;
                tempData.Contractors = AG.Contractors;
                tempData.CorrectiveSteps = AG.CorrectiveSteps;
                tempData.RelevantSupportingDocumentation = AG.RelevantSupportingDocumentation;
            }
            database.SaveChanges();
        }
    }
}