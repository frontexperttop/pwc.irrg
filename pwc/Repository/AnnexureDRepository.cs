﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pwc.Models;

namespace pwc.Repository
{
    public class AnnexureDRepository
    {
        private PWCContext database { get; set; }
        public AnnexureDRepository(PWCContext context)
        {
            this.database = context;
        }
        public AnnexureD GetByCaseID(int CaseID)
        {
            var result = database.AnnexureDs.AsQueryable();
            result = result.Where(x => x.CaseID == CaseID);
            if (result.Count() > 0)
                return result.First();
            else
                return null;
        }
        public void Save(AnnexureD AD)
        {
            var tempData = GetByCaseID(AD.CaseID ?? 0);
            if (tempData == null)
            {
                database.AnnexureDs.Add(AD);
            }
            else
            {
                tempData.D_Description = AD.D_Description;
                tempData.D_NamesOfOfficial = AD.D_NamesOfOfficial;
                tempData.D_Reason = AD.D_Reason;
            }
            database.SaveChanges();
        }
    }
}