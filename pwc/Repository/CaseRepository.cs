﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pwc.Models;
using System.Web.Mvc;

namespace pwc.Repository
{
    public class CaseRepository
    {
        private PWCContext database { get; set; }
        public CaseRepository(PWCContext context)
        {
            this.database = context;
        }
        public Summary GetDashboard()
        {
            Summary dashboard = new Summary();

            var result = database.Cases.AsQueryable();
            result = result.Where(x => x.Status != 0);
            dashboard.TotalIRR = result.Count();

            result = database.Cases.AsQueryable();
            result = result.Where(x => x.Status != 0);
            result = result.Where(x => x.Loss == "Yes");
            dashboard.LossIRR = result.Count();

            result = database.Cases.AsQueryable();
            result = result.Where(x => x.Status != 0);
            result = result.Where(x => x.OfficialDetermining == "Yes");
            dashboard.OfficialsInLaw = result.Count();

            result = database.Cases.AsQueryable();
            result = result.Where(x => x.Status != 0);
            result = result.Where(x => x.Amount <= 2000);
            dashboard.UnsolicitedBids = result.Count();

            result = database.Cases.AsQueryable();
            result = result.Where(x => x.Status != 0);
            result = result.Where(x => x.Amount > 2000);
            result = result.Where(x => x.Amount <= 10000);
            dashboard.R10K = result.Count();

            result = database.Cases.AsQueryable();
            result = result.Where(x => x.Status != 0);
            result = result.Where(x => x.Amount > 10000);
            result = result.Where(x => x.Amount <= 30000);
            dashboard.R30K = result.Count();

            result = database.Cases.AsQueryable();
            result = result.Where(x => x.Status != 0);
            result = result.Where(x => x.Amount > 30000);
            result = result.Where(x => x.Amount <= 500000);
            dashboard.R500K = result.Count();

            result = database.Cases.AsQueryable();
            result = result.Where(x => x.Status != 0);
            result = result.Where(x => x.Amount > 500000);
            dashboard.AboveR500K = result.Count();

            result = database.Cases.AsQueryable();
            result = result.Where(x => x.Status != 0);
            dashboard.LastIRRs = result.OrderByDescending(x => x.CaseID).Take(5).ToList<Case>();

            return dashboard;
        }
        public List<Case> GetCases()
        {
            return database.Cases.ToList<Case>();
        }
        public Case GetCaseByID(int ID)
        {
            return database.Cases.FindAsync(ID).Result;
        }
        public decimal TotalAmountOfPayments(int CaseID)
        {
            var payments = GetCaseByID(CaseID).Payments;
            decimal result = 0;
            if (payments != null)
            {
                foreach (Payment payment in payments)
                {
                    result += (payment.PaymentAmount ?? 0);
                }
            }
            return result;
        }
        public void SaveCase(Case caseData)
        {
            var tempCase = GetCaseTempByID(caseData.UserId);
            if(tempCase == null)
            {
                database.Cases.Add(caseData);
            }
            else
            {
                if (caseData.CaseNumber != null)
                    tempCase.CaseNumber = caseData.CaseNumber;
                if (caseData.OrderNumber != null)
                    tempCase.OrderNumber = caseData.OrderNumber;
                if (caseData.ContactRefferenceNumber != null)
                    tempCase.ContactRefferenceNumber = caseData.ContactRefferenceNumber;
                if (caseData.ContactValueIncVAT != null)
                    tempCase.ContactValueIncVAT = caseData.ContactValueIncVAT;
                if (caseData.PayeeOrBenificiery != null)
                    tempCase.PayeeOrBenificiery = caseData.PayeeOrBenificiery;
                if (caseData.Amount != null)
                    tempCase.Amount = caseData.Amount;
                if (caseData.Description != null)
                    tempCase.Description = caseData.Description;
                if (caseData.CategoryID != null)
                {
                    if (caseData.CategoryID == 0)
                    {
                        CategoryRepository repo = new CategoryRepository(new PWCContext());
                        Category newCategory = repo.AddCategory(caseData.NewCategory);
                        tempCase.CategoryID = newCategory.CategoryID;
                    }
                    else
                    {
                        tempCase.CategoryID = caseData.CategoryID;
                    }
                }
                if (caseData.Feasibility != null)
                    tempCase.Feasibility = caseData.Feasibility;
                if (caseData.InnovativeDesign != null)
                    tempCase.InnovativeDesign = caseData.InnovativeDesign;
                if (caseData.InnovativeApproach != null)
                    tempCase.InnovativeApproach = caseData.InnovativeApproach;
                if (caseData.EffectiveMethod != null)
                    tempCase.EffectiveMethod = caseData.EffectiveMethod;
                if (caseData.OtherSuppliers != null)
                    tempCase.OtherSuppliers = caseData.OtherSuppliers;
                if (caseData.UnsolicitedBidConclusion != null)
                    tempCase.UnsolicitedBidConclusion = caseData.UnsolicitedBidConclusion;
                if (caseData.OneQuoteAttached != null)
                    tempCase.OneQuoteAttached = caseData.OneQuoteAttached;
                if (caseData.OneQuoteAttachedRemarks != null)
                    tempCase.OneQuoteAttachedRemarks = caseData.OneQuoteAttachedRemarks;
                if (caseData.ThreeQuotesAttached != null)
                    tempCase.ThreeQuotesAttached = caseData.ThreeQuotesAttached;
                if (caseData.ThreeQuotesAttachedRemarks != null)
                    tempCase.ThreeQuotesAttachedRemarks = caseData.ThreeQuotesAttachedRemarks;
                if (caseData.RelateToContract != null)
                    tempCase.RelateToContract = caseData.RelateToContract;
                if (caseData.RelateToContractRemarks != null)
                    tempCase.RelateToContractRemarks = caseData.RelateToContractRemarks;
                if (caseData.TransversalContract != null)
                    tempCase.TransversalContract = caseData.TransversalContract;
                if (caseData.TransversalContractRemarks != null)
                    tempCase.TransversalContractRemarks = caseData.TransversalContractRemarks;
                if (caseData.Deviation != null)
                    tempCase.Deviation = caseData.Deviation;
                if (caseData.DeviationRemarks != null)
                    tempCase.DeviationRemarks = caseData.DeviationRemarks;
                if (caseData.WinningSupplier != null)
                    tempCase.WinningSupplier = caseData.WinningSupplier;
                if (caseData.WinningSupplierRemarks != null)
                    tempCase.WinningSupplierRemarks = caseData.WinningSupplierRemarks;
                if (caseData.PaymentConclusion != null)
                    tempCase.PaymentConclusion = caseData.PaymentConclusion;
                if (caseData.LowestQuote != null)
                    tempCase.LowestQuote = caseData.LowestQuote;
                if (caseData.LowestQuoteRemarks != null)
                    tempCase.LowestQuoteRemarks = caseData.LowestQuoteRemarks;
                if (caseData.ApprovedDeviation != null)
                    tempCase.ApprovedDeviation = caseData.ApprovedDeviation;
                if (caseData.ApprovedDeviationRemarks != null)
                    tempCase.ApprovedDeviationRemarks = caseData.ApprovedDeviationRemarks;
                if (caseData.AdequatelyDocumentedDeviation != null)
                    tempCase.AdequatelyDocumentedDeviation = caseData.AdequatelyDocumentedDeviation;
                if (caseData.AdequatelyDocumentedDeviationRemarks != null)
                    tempCase.AdequatelyDocumentedDeviationRemarks = caseData.AdequatelyDocumentedDeviationRemarks;
                if (caseData.CalculatedLoss != null)
                    tempCase.CalculatedLoss = caseData.CalculatedLoss;
                if (caseData.AdditionalInformation != null)
                    tempCase.AdditionalInformation = caseData.AdditionalInformation;
                if (caseData.GoodsWereRendered != null)
                    tempCase.GoodsWereRendered = caseData.GoodsWereRendered;
                if (caseData.GoodsWereRenderedRemarks != null)
                    tempCase.GoodsWereRenderedRemarks = caseData.GoodsWereRenderedRemarks;
                if (caseData.AssetRegister != null)
                    tempCase.AssetRegister = caseData.AssetRegister;
                if (caseData.AssetRegisterRemarks != null)
                    tempCase.AssetRegisterRemarks = caseData.AssetRegisterRemarks;
                if (caseData.DefinitionOfExpenditure != null)
                    tempCase.DefinitionOfExpenditure = caseData.DefinitionOfExpenditure;
                if (caseData.DefinitionOfExpenditureRemarks != null)
                    tempCase.DefinitionOfExpenditureRemarks = caseData.DefinitionOfExpenditureRemarks;
                if (caseData.RelatedPaymentsLoss != null)
                    tempCase.RelatedPaymentsLoss = caseData.RelatedPaymentsLoss;
                if (caseData.RelatedPaymentsLossRemarks != null)
                    tempCase.RelatedPaymentsLossRemarks = caseData.RelatedPaymentsLossRemarks;
                if (caseData.PaymentMade != null)
                    tempCase.PaymentMade = caseData.PaymentMade;
                if (caseData.PaymentMadeRemarks != null)
                    tempCase.PaymentMadeRemarks = caseData.PaymentMadeRemarks;
                if (caseData.Loss != null)
                    tempCase.Loss = caseData.Loss;
                if (caseData.OfficialDetermining != null)
                    tempCase.OfficialDetermining = caseData.OfficialDetermining;
                if (caseData.OfficialDeterminingRemarks != null)
                    tempCase.OfficialDeterminingRemarks = caseData.OfficialDeterminingRemarks;
                if (caseData.OfficialNumber != null)
                    tempCase.OfficialNumber = caseData.OfficialNumber;
                if (caseData.OfficialName != null)
                    tempCase.OfficialName = caseData.OfficialName;
                if (caseData.Possition != null)
                    tempCase.Possition = caseData.Possition;
                if (caseData.Role != null)
                    tempCase.Role = caseData.Role;
                if (caseData.ExceedPowers != null)
                    tempCase.ExceedPowers = caseData.ExceedPowers;
                if (caseData.ExceedPowersRemarks != null)
                    tempCase.ExceedPowersRemarks = caseData.ExceedPowersRemarks;
                if (caseData.UseOfAlcohol != null)
                    tempCase.UseOfAlcohol = caseData.UseOfAlcohol;
                if (caseData.UseOfAlcoholRemarks != null)
                    tempCase.UseOfAlcoholRemarks = caseData.UseOfAlcoholRemarks;
                if (caseData.ActInScope != null)
                    tempCase.ActInScope = caseData.ActInScope;
                if (caseData.ActInScopeRemarks != null)
                    tempCase.ActInScopeRemarks = caseData.ActInScopeRemarks;
                if (caseData.ActRectlessly != null)
                    tempCase.ActRectlessly = caseData.ActRectlessly;
                if (caseData.ActRectlesslyRemarks != null)
                    tempCase.ActRectlesslyRemarks = caseData.ActRectlesslyRemarks;
                if (caseData.WithoutPriorConsultation != null)
                    tempCase.WithoutPriorConsultation = caseData.WithoutPriorConsultation;
                if (caseData.WithoutPriorConsultationRemarks != null)
                    tempCase.WithoutPriorConsultationRemarks = caseData.WithoutPriorConsultationRemarks;
                if (caseData.OfficialFail != null)
                    tempCase.OfficialFail = caseData.OfficialFail;
                if (caseData.OfficialFailRemarks != null)
                    tempCase.OfficialFailRemarks = caseData.OfficialFailRemarks;
                if (caseData.OfficialConclusion != null)
                    tempCase.OfficialConclusion = caseData.OfficialConclusion;
                if (caseData.MeetIrregularExpenditure != null)
                    tempCase.MeetIrregularExpenditure = caseData.MeetIrregularExpenditure;
                if (caseData.MeetIrregularExpenditureRemarks != null)
                    tempCase.MeetIrregularExpenditureRemarks = caseData.MeetIrregularExpenditureRemarks;
                if (caseData.CondoningAuthority != null)
                    tempCase.CondoningAuthority = caseData.CondoningAuthority;
                if (caseData.TotalRandValue != null)
                    tempCase.TotalRandValue = caseData.TotalRandValue;

                tempCase.Status = caseData.Status;
            }

            database.SaveChanges();
        }
        public Case GetCaseTempByID(string UserID)
        {
            var result = database.Cases.AsQueryable();
            result = result.Where(x => x.UserId == UserID);
            result = result.Where(x => x.Status == 0);
            if (result.Count() > 0)
            {
                Case temp = result.First();
                return temp;
            }
            else
                return null;
        }
    }
}