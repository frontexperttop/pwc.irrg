﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pwc.Models;

namespace pwc.Repository
{
    public class CategoryRepository
    {
        private PWCContext database { get; set; }
        public CategoryRepository(PWCContext context)
        {
            this.database = context;
        }
        public List<Category> GetCategories()
        {
            List<Category> result = database.Categories.ToList<Category>();
            result.Add(new Category() { CategoryID = 0, CategoryName = "Other" });
            return result;
        }
        public Category GetCategoryByID(string ID)
        {
            return database.Categories.FindAsync(ID).Result;
        }
        public Category AddCategory(string categoryName)
        {
            Category result = new Category();
            result.CategoryName = categoryName;
            database.Categories.Add(result);
            database.SaveChanges();
            return result;
        }
    }
}