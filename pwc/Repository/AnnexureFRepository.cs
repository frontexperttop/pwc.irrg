﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pwc.Models;

namespace pwc.Repository
{
    public class AnnexureFRepository
    {
        private PWCContext database { get; set; }
        public AnnexureFRepository(PWCContext context)
        {
            this.database = context;
        }
        public AnnexureF GetByCaseID(int CaseID)
        {
            var result = database.AnnexureFs.AsQueryable();
            result = result.Where(x => x.CaseID == CaseID);
            if (result.Count() > 0)
                return result.First();
            else
                return null;
        }
        public void Save(AnnexureF AF)
        {
            var tempData = GetByCaseID(AF.CaseID ?? 0);
            if (tempData == null)
            {
                database.AnnexureFs.Add(AF);
            }
            else
            {
                tempData.F_DateOfFirstPayment = AF.F_DateOfFirstPayment;
                tempData.F_DateOfLastPayment = AF.F_DateOfLastPayment;
                tempData.F_Description = AF.F_Description;
                tempData.F_OfficialName = AF.F_OfficialName;
                tempData.F_Position = AF.F_Position;
                tempData.F_Role = AF.F_Role;
                tempData.F_InstructionNotCompliedWith = AF.F_InstructionNotCompliedWith;
                tempData.F_LossAmount = AF.F_LossAmount;
            }
            database.SaveChanges();
        }
    }
}