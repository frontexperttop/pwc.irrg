﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pwc.Models;

namespace pwc.Repository
{
    public class PaymentRepository
    {
        private PWCContext database { get; set; }
        public PaymentRepository(PWCContext context)
        {
            this.database = context;
        }
        public Payment GetPayment(int PaymentID)
        {
            return database.Payments.Find(PaymentID);
        }
        public DateTime GetDateOfFirstPayment(int CaseID)
        {
            var result = database.Payments.AsQueryable();
            result = result.Where(x => x.CaseID == CaseID);
            result = result.OrderBy(x => x.PaymentDate);
            return result.FirstOrDefault().PaymentDate;
        }
        public DateTime GetDateOfLastPayment(int CaseID)
        {
            var result = database.Payments.AsQueryable();
            result = result.Where(x => x.CaseID == CaseID);
            result = result.OrderByDescending(x => x.PaymentDate);
            return result.FirstOrDefault().PaymentDate; ;
        }
        public List<Payment> GetPayments(int CaseID)
        {
            var result = database.Payments.AsQueryable();
            result = result.Where(x => x.CaseID == CaseID);
            return result.ToList<Payment>();
        }
        public void DeletePayment(int PaymentID)
        {
            database.Payments.Remove(GetPayment(PaymentID));
            database.SaveChanges();
        }
        public void SavePayment(Payment payment)
        {
            database.Payments.Add(payment);
            database.SaveChanges();
        }
    }
}