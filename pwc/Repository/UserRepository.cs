﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pwc.Models;

namespace pwc.Repository
{
    public class UserRepository
    {
        private PWCContext database { get; set; }
        public UserRepository(PWCContext context)
        {
            this.database = context;
        }

        public Nullable<int> Authentication(User userData)
        {
            //string EPass = Helper.ComputeHash(userData.UserPassword, "SHA512", null);
            //bool flag = Helper.VerifyHash(password, "SHA512", EPass);

            var result = database.Users.AsQueryable();
            result = result.Where(x => x.UserName == userData.UserName);
            if (result.Count() > 0)
            {
                bool flag = Helper.VerifyHash(userData.UserPassword, "SHA512", result.First().UserPassword);
                if (flag)
                {
                    return result.First().UserID;
                }
            }
            return null;
        }
    }
}