﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pwc.Models;

namespace pwc.Repository
{
    public class CaseFileRepository
    {
        private PWCContext database { get; set; }
        public CaseFileRepository(PWCContext context)
        {
            this.database = context;
        }
        public Payment GetCaseFile(int CaseFileID)
        {
            return database.Payments.Find(CaseFileID);
        }
        public void SaveCaseFile(CaseFile file)
        {
            database.CaseFiles.Add(file);
            database.SaveChanges();
        }
        public void DeleteCaseFile(int CaseFileID)
        {
            database.Payments.Remove(GetCaseFile(CaseFileID));
            database.SaveChanges();
        }
    }
}