﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pwc.Models;
using System.Data.Entity;

namespace pwc.Repository
{
    public class PWCContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Case> Cases { get; set; }
        public DbSet<CaseFile> CaseFiles { get; set; }
        public DbSet<AnnexureB> AnnexureBs { get; set; }
        public DbSet<AnnexureC> AnnexureCs { get; set; }
        public DbSet<AnnexureD> AnnexureDs { get; set; }
        public DbSet<AnnexureF> AnnexureFs { get; set; }
        public DbSet<AnnexureG> AnnexureGs { get; set; }
    }
}