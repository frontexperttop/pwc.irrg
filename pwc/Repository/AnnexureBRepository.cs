﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pwc.Models;

namespace pwc.Repository
{
    public class AnnexureBRepository
    {
        private PWCContext database { get; set; }
        public AnnexureBRepository(PWCContext context)
        {
            this.database = context;
        }
        public AnnexureB GetByCaseID(int CaseID)
        {
            var result = database.AnnexureBs.AsQueryable();
            result = result.Where(x => x.CaseID == CaseID);
            if (result.Count() > 0)
                return result.First();
            else
                return null;
        }
        public void Save(AnnexureB AB)
        {
            var tempData = GetByCaseID(AB.CaseID??0);
            if (tempData == null)
            {
                database.AnnexureBs.Add(AB);
            } else
            {
                tempData.B_Description = AB.B_Description;
                tempData.B_Reason = AB.B_Reason;
            }
            database.SaveChanges();
        }
    }
}