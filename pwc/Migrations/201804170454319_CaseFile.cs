namespace pwc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CaseFile : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CaseFiles",
                c => new
                    {
                        CaseFileID = c.Int(nullable: false, identity: true),
                        FieldName = c.String(),
                        FileName = c.String(),
                        FilePath = c.String(),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.CaseFileID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            AddColumn("dbo.Cases", "OneQuoteAttached", c => c.String());
            AddColumn("dbo.Cases", "ThreeQuotesAttached", c => c.String());
            AddColumn("dbo.Cases", "RelateToContract", c => c.String());
            AddColumn("dbo.Cases", "TransversalContract", c => c.String());
            AddColumn("dbo.Cases", "Deviation", c => c.String());
            DropColumn("dbo.Cases", "PaymentQuestionnaire");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cases", "PaymentQuestionnaire", c => c.String());
            DropForeignKey("dbo.CaseFiles", "CaseID", "dbo.Cases");
            DropIndex("dbo.CaseFiles", new[] { "CaseID" });
            DropColumn("dbo.Cases", "Deviation");
            DropColumn("dbo.Cases", "TransversalContract");
            DropColumn("dbo.Cases", "RelateToContract");
            DropColumn("dbo.Cases", "ThreeQuotesAttached");
            DropColumn("dbo.Cases", "OneQuoteAttached");
            DropTable("dbo.CaseFiles");
        }
    }
}
