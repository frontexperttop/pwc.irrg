namespace pwc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "OneQuoteAttachedRemarks", c => c.String());
            AddColumn("dbo.Cases", "ThreeQuotesAttachedRemarks", c => c.String());
            AddColumn("dbo.Cases", "RelateToContractRemarks", c => c.String());
            AddColumn("dbo.Cases", "TransversalContractRemarks", c => c.String());
            AddColumn("dbo.Cases", "DeviationRemarks", c => c.String());
            AddColumn("dbo.Cases", "WinningSupplier", c => c.String());
            AddColumn("dbo.Cases", "WinningSupplierRemarks", c => c.String());
            AddColumn("dbo.Cases", "LowestQuoteRemarks", c => c.String());
            AddColumn("dbo.Cases", "ApprovedDeviationRemarks", c => c.String());
            AddColumn("dbo.Cases", "AdequatelyDocumentedDeviationRemarks", c => c.String());
            AddColumn("dbo.Cases", "GoodsWereRenderedRemarks", c => c.String());
            AddColumn("dbo.Cases", "AssetRegisterRemarks", c => c.String());
            AddColumn("dbo.Cases", "DefinitionOfExpenditureRemarks", c => c.String());
            AddColumn("dbo.Cases", "RelatedPaymentsLossRemarks", c => c.String());
            AddColumn("dbo.Cases", "PaymentMadeRemarks", c => c.String());
            AddColumn("dbo.Cases", "OfficialDeterminingRemarks", c => c.String());
            AddColumn("dbo.Cases", "ExceedPowersRemarks", c => c.String());
            AddColumn("dbo.Cases", "UseOfAlcoholRemarks", c => c.String());
            AddColumn("dbo.Cases", "ActInScopeRemarks", c => c.String());
            AddColumn("dbo.Cases", "ActRectlesslyRemarks", c => c.String());
            AddColumn("dbo.Cases", "WithoutPriorConsultationRemarks", c => c.String());
            AddColumn("dbo.Cases", "OfficialFailRemarks", c => c.String());
            AddColumn("dbo.Cases", "MeetIrregularExpenditureRemarks", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "MeetIrregularExpenditureRemarks");
            DropColumn("dbo.Cases", "OfficialFailRemarks");
            DropColumn("dbo.Cases", "WithoutPriorConsultationRemarks");
            DropColumn("dbo.Cases", "ActRectlesslyRemarks");
            DropColumn("dbo.Cases", "ActInScopeRemarks");
            DropColumn("dbo.Cases", "UseOfAlcoholRemarks");
            DropColumn("dbo.Cases", "ExceedPowersRemarks");
            DropColumn("dbo.Cases", "OfficialDeterminingRemarks");
            DropColumn("dbo.Cases", "PaymentMadeRemarks");
            DropColumn("dbo.Cases", "RelatedPaymentsLossRemarks");
            DropColumn("dbo.Cases", "DefinitionOfExpenditureRemarks");
            DropColumn("dbo.Cases", "AssetRegisterRemarks");
            DropColumn("dbo.Cases", "GoodsWereRenderedRemarks");
            DropColumn("dbo.Cases", "AdequatelyDocumentedDeviationRemarks");
            DropColumn("dbo.Cases", "ApprovedDeviationRemarks");
            DropColumn("dbo.Cases", "LowestQuoteRemarks");
            DropColumn("dbo.Cases", "WinningSupplierRemarks");
            DropColumn("dbo.Cases", "WinningSupplier");
            DropColumn("dbo.Cases", "DeviationRemarks");
            DropColumn("dbo.Cases", "TransversalContractRemarks");
            DropColumn("dbo.Cases", "RelateToContractRemarks");
            DropColumn("dbo.Cases", "ThreeQuotesAttachedRemarks");
            DropColumn("dbo.Cases", "OneQuoteAttachedRemarks");
        }
    }
}
