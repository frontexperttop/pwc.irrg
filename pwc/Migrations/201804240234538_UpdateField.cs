namespace pwc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cases", "OrderNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cases", "OrderNumber");
        }
    }
}
