namespace pwc.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    using pwc.Models;
    using pwc.Helpers;

    internal sealed class Configuration : DbMigrationsConfiguration<PWCContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PWCContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            //string EPass = Helper.ComputeHash("test", "SHA512", null);
            //var Users = new List<User>
            //{
            //    new User
            //    {
            //        UserID = 1,
            //        UserName = "test1",
            //        UserPassword = EPass
            //    },
            //};
            //Users.ForEach(s => context.Users.AddOrUpdate(p => p.UserName, s));
            //context.SaveChanges();

            var Categories = new List<Category>
            {
                new Category
                {
                    CategoryID = 1,
                    CategoryName = "Bidding process(tenders) not followed"
                },
                new Category
                {
                    CategoryID = 2,
                    CategoryName = "Deviation from normal bidding process"
                },
                new Category
                {
                    CategoryID = 3,
                    CategoryName = "Tranaction not approved by correctly delegated officials"
                },
                new Category
                {
                    CategoryID = 4,
                    CategoryName = "Prohibited suppliers"
                },
                new Category
                {
                    CategoryID = 5,
                    CategoryName = "SCM process (quotations) not followed"
                },
                new Category
                {
                    CategoryID = 6,
                    CategoryName = "Spending not in terms of the conditional grant"
                },
                new Category
                {
                    CategoryID = 7,
                    CategoryName = "Contractors: Non-compliance with procurement of infrastructure contracts"
                },
                new Category
                {
                    CategoryID = 8,
                    CategoryName = "Non-compliance with SCM procedures"
                },
                new Category
                {
                    CategoryID = 9,
                    CategoryName = "Payments not authorised per financial delegations"
                },
                new Category
                {
                    CategoryID = 10,
                    CategoryName = "Bids advertised for less than 21 days"
                },
                new Category
                {
                    CategoryID = 11,
                    CategoryName = "Suppliers in service of state"
                },
                new Category
                {
                    CategoryID = 12,
                    CategoryName = "Declaration of interest & Tax clearance certificates"
                },
                new Category
                {
                    CategoryID = 13,
                    CategoryName = "Variation orders exceeding 20% of contract amount"
                },
                new Category
                {
                    CategoryID = 14,
                    CategoryName = "Investigations not done within 90 days"
                },
                new Category
                {
                    CategoryID = 15,
                    CategoryName = "Contracts previously recorded as irregular"
                },
                new Category
                {
                    CategoryID = 16,
                    CategoryName = "Events management transversal contract � invoices does not agree"
                },
                new Category
                {
                    CategoryID = 17,
                    CategoryName = "Events management transversal contract � items not on signed contract"
                },
                new Category
                {
                    CategoryID = 18,
                    CategoryName = "Participation in National Transversal contract for rental of office equipment"
                },
                new Category
                {
                    CategoryID = 19,
                    CategoryName = "Services rendered without orders"
                },
                new Category
                {
                    CategoryID = 20,
                    CategoryName = "Multi-year Partnership payments"
                },
                new Category
                {
                    CategoryID = 21,
                    CategoryName = "Non-compliance with SCM procedures � International Travel"
                },
                new Category
                {
                    CategoryID = 22,
                    CategoryName = "Deviations not according to laws & regulations"
                },
                new Category
                {
                    CategoryID = 23,
                    CategoryName = "Quotations not obtained from 3 suppliers"
                },
                new Category
                {
                    CategoryID = 24,
                    CategoryName = "Irregular contracts identified by DPW"
                },
                new Category
                {
                    CategoryID = 25,
                    CategoryName = "Possible collusion between bidders"
                },
                new Category
                {
                    CategoryID = 26,
                    CategoryName = "Misrepresentation of information"
                },
                new Category
                {
                    CategoryID = 27,
                    CategoryName = "Procurement & contract management: Interest in suppliers"
                },
            };
            Categories.ForEach(s => context.Categories.AddOrUpdate(p => p.CategoryName, s));
            context.SaveChanges();

            //var Instructions = new List<InstructionNotCompliedWith>
            //{
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 1,
            //        InstructionName = "PN 8 of 2007/08 par 3.2 and TR16A6.1"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 2,
            //        InstructionName = "PN 8 of 2007/08 par 3.3.1"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 3,
            //        InstructionName = "TR16A6.1"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 4,
            //        InstructionName = "TR16A6.1"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 5,
            //        InstructionName = "PN 8 of 2007/08 par 3.3.3"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 6,
            //        InstructionName = "TR16A6.4"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 7,
            //        InstructionName = "TR 16A6.3(b)"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 8,
            //        InstructionName = "PPPFA Sec 2(1)(a)"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 9,
            //        InstructionName = "PN 8 of 2007/08 par 3.3.3 & 3.3.2"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 10,
            //        InstructionName = "Treasury Instruction 4A of 2016/17"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 11,
            //        InstructionName = "TR 16A3.2(a) (fairness)"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 12,
            //        InstructionName = "PPR 14"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 13,
            //        InstructionName = "TR16A9.1(d)"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 14,
            //        InstructionName = "TR16A6.5"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 15,
            //        InstructionName = "PN 7 of 2009/10 (SBD 4)"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 16,
            //        InstructionName = "TR 16A6.3(c)"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 17,
            //        InstructionName = "TR 16A6.2(a)"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 18,
            //        InstructionName = "TR 16A6.2(b)"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 19,
            //        InstructionName = "Treasury instruction note 1 of 2013/14 (1 January 2014) Cost containment no 4.15"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 20,
            //        InstructionName = "Treasury instruction note 1 of 2013/14 (1 January 2014) Cost containment no 4.17"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 21,
            //        InstructionName = "Treasury instruction note 1 of 2013/14 (1 January 2014) Cost containment no 4.23"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 22,
            //        InstructionName = "Treasury instruction note 1 of 2013/14 (1 January 2014) Cost containment no 4.24"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 23,
            //        InstructionName = "Treasury instruction note 1 of 2013/14 (1 January 2014) Cost containment no 4.25"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 24,
            //        InstructionName = "Treasury instruction note 2 of 2016/17 (1 November 2016) Cost containment no 4.14"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 25,
            //        InstructionName = "Treasury instruction note 2 of 2016/17 (1 November 2016) Cost containment no 4.16"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 26,
            //        InstructionName = "Treasury instruction note 2 of 2016/17 (1 November 2016) Cost containment no 4.17"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 27,
            //        InstructionName = "Treasury instruction note 2 of 2016/17 (1 November 2016) Cost containment no 4.18"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 28,
            //        InstructionName = "Treasury instruction note 2 of 2016/17 (1 November 2016) Cost containment no 4.19"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 29,
            //        InstructionName = "Treasury instruction note 2 of 2016/17 (1 November 2016) Cost containment no 4.20"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 30,
            //        InstructionName = "Treasury instruction note 2 of 2016/17 (1 November 2016) Cost containment no 4.30"
            //    },
            //    new InstructionNotCompliedWith
            //    {
            //        InstructionNotCompliedWithID = 31,
            //        InstructionName = "Treasury instruction note 3 of 2016/17 (1 November 2016) Cost containment"
            //    },
            //};
            //Instructions.ForEach(s => context.Instructions.AddOrUpdate(p => p.InstructionName, s));
            //context.SaveChanges();
        }
    }
}
