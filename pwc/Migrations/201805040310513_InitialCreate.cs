namespace pwc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnnexureBs",
                c => new
                    {
                        AnnexureBID = c.Int(nullable: false, identity: true),
                        B_Description = c.String(),
                        B_Reason = c.String(),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.AnnexureBID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.Cases",
                c => new
                    {
                        CaseID = c.Int(nullable: false, identity: true),
                        CaseNumber = c.String(),
                        OrderNumber = c.String(),
                        ContactRefferenceNumber = c.String(),
                        ContactValueIncVAT = c.Decimal(precision: 18, scale: 2),
                        PayeeOrBenificiery = c.String(),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        Description = c.String(),
                        CategoryID = c.Int(),
                        Feasibility = c.String(),
                        InnovativeDesign = c.String(),
                        InnovativeApproach = c.String(),
                        EffectiveMethod = c.String(),
                        OtherSuppliers = c.String(),
                        UnsolicitedBidConclusion = c.String(),
                        OneQuoteAttached = c.String(),
                        OneQuoteAttachedRemarks = c.String(),
                        ThreeQuotesAttached = c.String(),
                        ThreeQuotesAttachedRemarks = c.String(),
                        RelateToContract = c.String(),
                        RelateToContractRemarks = c.String(),
                        TransversalContract = c.String(),
                        TransversalContractRemarks = c.String(),
                        Deviation = c.String(),
                        DeviationRemarks = c.String(),
                        WinningSupplier = c.String(),
                        WinningSupplierRemarks = c.String(),
                        PaymentConclusion = c.String(),
                        LowestQuote = c.String(),
                        LowestQuoteRemarks = c.String(),
                        ApprovedDeviation = c.String(),
                        ApprovedDeviationRemarks = c.String(),
                        AdequatelyDocumentedDeviation = c.String(),
                        AdequatelyDocumentedDeviationRemarks = c.String(),
                        CalculatedLoss = c.Decimal(precision: 18, scale: 2),
                        AdditionalInformation = c.String(),
                        GoodsWereRendered = c.String(),
                        GoodsWereRenderedRemarks = c.String(),
                        AssetRegister = c.String(),
                        AssetRegisterRemarks = c.String(),
                        DefinitionOfExpenditure = c.String(),
                        DefinitionOfExpenditureRemarks = c.String(),
                        RelatedPaymentsLoss = c.String(),
                        RelatedPaymentsLossRemarks = c.String(),
                        PaymentMade = c.String(),
                        PaymentMadeRemarks = c.String(),
                        Loss = c.String(),
                        OfficialDetermining = c.String(),
                        OfficialDeterminingRemarks = c.String(),
                        OfficialNumber = c.Int(),
                        OfficialName = c.String(),
                        Possition = c.String(),
                        Role = c.String(),
                        ExceedPowers = c.String(),
                        ExceedPowersRemarks = c.String(),
                        UseOfAlcohol = c.String(),
                        UseOfAlcoholRemarks = c.String(),
                        ActInScope = c.String(),
                        ActInScopeRemarks = c.String(),
                        ActRectlessly = c.String(),
                        ActRectlesslyRemarks = c.String(),
                        WithoutPriorConsultation = c.String(),
                        WithoutPriorConsultationRemarks = c.String(),
                        OfficialFail = c.String(),
                        OfficialFailRemarks = c.String(),
                        OfficialConclusion = c.String(),
                        MeetIrregularExpenditure = c.String(),
                        MeetIrregularExpenditureRemarks = c.String(),
                        CondoningAuthority = c.String(),
                        TotalRandValue = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.CaseID)
                .ForeignKey("dbo.Categories", t => t.CategoryID)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.CategoryID)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AnnexureCs",
                c => new
                    {
                        AnnexureCID = c.Int(nullable: false, identity: true),
                        C_Description = c.String(),
                        C_Involved = c.String(),
                        C_Reason = c.String(),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.AnnexureCID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.AnnexureDs",
                c => new
                    {
                        AnnexureDID = c.Int(nullable: false, identity: true),
                        D_Description = c.String(),
                        D_NamesOfOfficial = c.String(),
                        D_Reason = c.String(),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.AnnexureDID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.AnnexureFs",
                c => new
                    {
                        AnnexureFID = c.Int(nullable: false, identity: true),
                        F_DateOfFirstPayment = c.DateTime(),
                        F_DateOfLastPayment = c.DateTime(),
                        F_Description = c.String(),
                        F_OfficialName = c.String(),
                        F_Position = c.String(),
                        F_Role = c.String(),
                        F_InstructionNotCompliedWith = c.String(),
                        F_LossAmount = c.Decimal(precision: 18, scale: 2),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.AnnexureFID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.AnnexureGs",
                c => new
                    {
                        AnnexureGID = c.Int(nullable: false, identity: true),
                        EvidenceIndicating = c.String(),
                        TransgressionReason = c.String(),
                        DetailedMotivation = c.String(),
                        DetailOfTransgression = c.String(),
                        ReferenceToRelevantLegislation = c.String(),
                        DeviatingReason = c.String(),
                        IndicateStateLoss = c.String(),
                        FromOneBidder = c.String(),
                        FinancialImplications = c.String(),
                        Contractors = c.String(),
                        CorrectiveSteps = c.String(),
                        RelevantSupportingDocumentation = c.String(),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.AnnexureGID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.CaseFiles",
                c => new
                    {
                        CaseFileID = c.Int(nullable: false, identity: true),
                        FieldName = c.String(),
                        FileName = c.String(),
                        FilePath = c.String(),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.CaseFileID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        PaymentID = c.Int(nullable: false, identity: true),
                        PaymentNumber = c.String(),
                        OrderNumber = c.String(),
                        PaymentDate = c.DateTime(nullable: false),
                        PaymentAmount = c.Decimal(precision: 18, scale: 2),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.PaymentID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Cases", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Payments", "CaseID", "dbo.Cases");
            DropForeignKey("dbo.Cases", "CategoryID", "dbo.Categories");
            DropForeignKey("dbo.CaseFiles", "CaseID", "dbo.Cases");
            DropForeignKey("dbo.AnnexureGs", "CaseID", "dbo.Cases");
            DropForeignKey("dbo.AnnexureFs", "CaseID", "dbo.Cases");
            DropForeignKey("dbo.AnnexureDs", "CaseID", "dbo.Cases");
            DropForeignKey("dbo.AnnexureCs", "CaseID", "dbo.Cases");
            DropForeignKey("dbo.AnnexureBs", "CaseID", "dbo.Cases");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Payments", new[] { "CaseID" });
            DropIndex("dbo.CaseFiles", new[] { "CaseID" });
            DropIndex("dbo.AnnexureGs", new[] { "CaseID" });
            DropIndex("dbo.AnnexureFs", new[] { "CaseID" });
            DropIndex("dbo.AnnexureDs", new[] { "CaseID" });
            DropIndex("dbo.AnnexureCs", new[] { "CaseID" });
            DropIndex("dbo.Cases", new[] { "UserId" });
            DropIndex("dbo.Cases", new[] { "CategoryID" });
            DropIndex("dbo.AnnexureBs", new[] { "CaseID" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Payments");
            DropTable("dbo.Categories");
            DropTable("dbo.CaseFiles");
            DropTable("dbo.AnnexureGs");
            DropTable("dbo.AnnexureFs");
            DropTable("dbo.AnnexureDs");
            DropTable("dbo.AnnexureCs");
            DropTable("dbo.Cases");
            DropTable("dbo.AnnexureBs");
        }
    }
}
