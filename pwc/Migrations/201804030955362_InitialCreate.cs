namespace pwc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnnexureBs",
                c => new
                    {
                        AnnexureBID = c.Int(nullable: false, identity: true),
                        B_Description = c.String(),
                        B_Reason = c.String(),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.AnnexureBID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.Cases",
                c => new
                    {
                        CaseID = c.Int(nullable: false, identity: true),
                        CaseNumber = c.String(),
                        ContactRefferenceNumber = c.String(),
                        ContactValueIncVAT = c.Decimal(precision: 18, scale: 2),
                        PayeeOrBenificiery = c.String(),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        Description = c.String(),
                        CategoryID = c.Int(),
                        Feasibility = c.String(),
                        InnovativeDesign = c.String(),
                        InnovativeApproach = c.String(),
                        EffectiveMethod = c.String(),
                        OtherSuppliers = c.String(),
                        UnsolicitedBidConclusion = c.String(),
                        PaymentQuestionnaire = c.String(),
                        PaymentConclusion = c.String(),
                        LowestQuote = c.String(),
                        ApprovedDeviation = c.String(),
                        AdequatelyDocumentedDeviation = c.String(),
                        CalculatedLoss = c.Decimal(precision: 18, scale: 2),
                        AdditionalInformation = c.String(),
                        GoodsWereRendered = c.String(),
                        AssetRegister = c.String(),
                        DefinitionOfExpenditure = c.String(),
                        RelatedPaymentsLoss = c.String(),
                        PaymentMade = c.String(),
                        Loss = c.String(),
                        OfficialDetermining = c.String(),
                        OfficialNumber = c.Int(),
                        OfficialName = c.String(),
                        Possition = c.String(),
                        Role = c.String(),
                        ExceedPowers = c.String(),
                        UseOfAlcohol = c.String(),
                        ActInScope = c.String(),
                        ActRectlessly = c.String(),
                        WithoutPriorConsultation = c.String(),
                        OfficialFail = c.String(),
                        OfficialConclusion = c.String(),
                        MeetIrregularExpenditure = c.String(),
                        CondoningAuthority = c.String(),
                        TotalRandValue = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CaseID)
                .ForeignKey("dbo.Categories", t => t.CategoryID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.CategoryID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.AnnexureCs",
                c => new
                    {
                        AnnexureCID = c.Int(nullable: false, identity: true),
                        C_Description = c.String(),
                        C_Involved = c.String(),
                        C_Reason = c.String(),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.AnnexureCID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.AnnexureDs",
                c => new
                    {
                        AnnexureDID = c.Int(nullable: false, identity: true),
                        D_Description = c.String(),
                        D_NamesOfOfficial = c.String(),
                        D_Reason = c.String(),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.AnnexureDID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.AnnexureFs",
                c => new
                    {
                        AnnexureFID = c.Int(nullable: false, identity: true),
                        F_DateOfFirstPayment = c.DateTime(),
                        F_DateOfLastPayment = c.DateTime(),
                        F_Description = c.String(),
                        F_OfficialName = c.String(),
                        F_Position = c.String(),
                        F_Role = c.String(),
                        F_InstructionNotCompliedWith = c.String(),
                        F_LossAmount = c.Decimal(precision: 18, scale: 2),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.AnnexureFID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.AnnexureGs",
                c => new
                    {
                        AnnexureGID = c.Int(nullable: false, identity: true),
                        EvidenceIndicating = c.String(),
                        TransgressionReason = c.String(),
                        DetailedMotivation = c.String(),
                        DetailOfTransgression = c.String(),
                        ReferenceToRelevantLegislation = c.String(),
                        DeviatingReason = c.String(),
                        IndicateStateLoss = c.String(),
                        FromOneBidder = c.String(),
                        FinancialImplications = c.String(),
                        Contractors = c.String(),
                        CorrectiveSteps = c.String(),
                        RelevantSupportingDocumentation = c.String(),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.AnnexureGID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryID = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        PaymentID = c.Int(nullable: false, identity: true),
                        PaymentNumber = c.String(),
                        OrderNumber = c.String(),
                        PaymentDate = c.DateTime(nullable: false),
                        PaymentAmount = c.Decimal(precision: 18, scale: 2),
                        CaseID = c.Int(),
                    })
                .PrimaryKey(t => t.PaymentID)
                .ForeignKey("dbo.Cases", t => t.CaseID)
                .Index(t => t.CaseID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        UserPassword = c.String(),
                    })
                .PrimaryKey(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cases", "UserID", "dbo.Users");
            DropForeignKey("dbo.Payments", "CaseID", "dbo.Cases");
            DropForeignKey("dbo.Cases", "CategoryID", "dbo.Categories");
            DropForeignKey("dbo.AnnexureGs", "CaseID", "dbo.Cases");
            DropForeignKey("dbo.AnnexureFs", "CaseID", "dbo.Cases");
            DropForeignKey("dbo.AnnexureDs", "CaseID", "dbo.Cases");
            DropForeignKey("dbo.AnnexureCs", "CaseID", "dbo.Cases");
            DropForeignKey("dbo.AnnexureBs", "CaseID", "dbo.Cases");
            DropIndex("dbo.Payments", new[] { "CaseID" });
            DropIndex("dbo.AnnexureGs", new[] { "CaseID" });
            DropIndex("dbo.AnnexureFs", new[] { "CaseID" });
            DropIndex("dbo.AnnexureDs", new[] { "CaseID" });
            DropIndex("dbo.AnnexureCs", new[] { "CaseID" });
            DropIndex("dbo.Cases", new[] { "UserID" });
            DropIndex("dbo.Cases", new[] { "CategoryID" });
            DropIndex("dbo.AnnexureBs", new[] { "CaseID" });
            DropTable("dbo.Users");
            DropTable("dbo.Payments");
            DropTable("dbo.Categories");
            DropTable("dbo.AnnexureGs");
            DropTable("dbo.AnnexureFs");
            DropTable("dbo.AnnexureDs");
            DropTable("dbo.AnnexureCs");
            DropTable("dbo.Cases");
            DropTable("dbo.AnnexureBs");
        }
    }
}
