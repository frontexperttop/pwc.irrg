﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

using Microsoft.AspNet.Identity.EntityFramework;

using pwc.Models;

namespace pwc
{
    public class PWCContext : IdentityDbContext<ApplicationUser, ApplicationRole,
        string, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public PWCContext()
            : base("PWCContext")
        {

        }

        static PWCContext()
        {
            Database.SetInitializer<PWCContext>(new ApplicationDbInitializer());
        }

        public static PWCContext Create()
        {
            return new PWCContext();
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Case> Cases { get; set; }
        public DbSet<CaseFile> CaseFiles { get; set; }
        public DbSet<AnnexureB> AnnexureBs { get; set; }
        public DbSet<AnnexureC> AnnexureCs { get; set; }
        public DbSet<AnnexureD> AnnexureDs { get; set; }
        public DbSet<AnnexureF> AnnexureFs { get; set; }
        public DbSet<AnnexureG> AnnexureGs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Case>()
                        .HasRequired(n => n.User)
                        .WithMany(a => a.Cases)
                        .HasForeignKey(n => n.UserId)
                        .WillCascadeOnDelete(false);
        }
    }
}