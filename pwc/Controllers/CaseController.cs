﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.IO;
using System.Data;

using LumenWorks.Framework.IO.Csv;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

using pwc.Models;
using pwc.Repository;

namespace pwc.Controllers
{
    public class CaseController : Controller
    {
        private PWCContext context = new PWCContext();

        public bool FixLinks(int count)
        {
            List<string> links = (List<string>)TempData["LINK"];
            List<string> urls = (List<string>)TempData["URL"];
            if (links.Count < count)
            {
                return false;
            }
            while (links.Count > count)
            {
                links.RemoveAt(links.Count - 1);
                urls.RemoveAt(urls.Count - 1);
            }
            TempData["LINK"] = links;
            TempData["URL"] = urls;
            return true;
        }
        public ActionResult Phase1()
        {
            //if (Session["UserID"] == null)
            //{
            //    return RedirectToAction("Index", "Login");
            //}

            string UserID = User.Identity.GetUserId();

            CaseRepository caseRepo = new CaseRepository(context);
            Case tempCase = caseRepo.GetCaseTempByID(UserID);
            if (tempCase == null)
            {
                Case caseModel = new Case();
                caseModel.UserId = UserID.ToString();
                caseRepo.SaveCase(caseModel);
                tempCase = caseModel;
            }

            var categories = new CategoryRepository(context).GetCategories();

            var links = new List<string>
            {
                "Phase 1",
                "Open A Case"
            };
            var urls = new List<string>
            {
                "Phase1",
                "Phase1"
            };
            TempData["LINK"] = links;
            TempData["URL"] = urls;
            ViewBag.links = links;
            ViewBag.urls = urls;

            ViewBag.categories = categories;
            ViewBag.totalAmount = caseRepo.TotalAmountOfPayments(tempCase.CaseID);
            tempCase.CategoryList = new SelectList(new CategoryRepository(context).GetCategories(), "CategoryID", "CategoryName");
            return View(tempCase);
        }

        public ActionResult UnsolicitedBids()
        {
            string UserID = User.Identity.GetUserId();

            CaseRepository caseRepo = new CaseRepository(context);
            Case tempCase = caseRepo.GetCaseTempByID(UserID);
            if (tempCase == null)
            {
                return RedirectToAction("Phase1", "Case");
            }

            var links = new List<string>
            {
                "Phase 1",
                "Unsolicited Bidse"
            };
            var urls = new List<string>
            {
                "Phase1",
                "UnsolicitedBids"
            };
            TempData["LINK"] = links;
            TempData["URL"] = urls;
            ViewBag.links = links;
            ViewBag.urls = urls;

            return View(tempCase);
        }

        public ActionResult Payments()
        {
            string UserID = User.Identity.GetUserId();

            CaseRepository caseRepo = new CaseRepository(context);
            Case tempCase = caseRepo.GetCaseTempByID(UserID);
            if (tempCase == null)
            {
                return RedirectToAction("Phase1", "Case");
            }
            List<string> links = (List<string>)TempData["LINK"];
            List<string> urls = (List<string>)TempData["URL"];

            decimal totalAmount = caseRepo.TotalAmountOfPayments(tempCase.CaseID);
            if (totalAmount < 10000)
            {
                links[1] = "Payment R2K and R10K";
                ViewBag.Header = "Answer few questions here (Payment > R2K AND < R10K): -";
                ViewBag.Header = "DETERMINE IRREGULAR EXPENDITURE FOR PAYMENTS GREATER THAN R 2000 AND LESS THAN R 10 000";
                ViewBag.Payment = "R10K";
            }
            else if (totalAmount < 30000)
            {
                links[1] = "Payment R10K and R30K";
                ViewBag.Header = "Answer few questions here (Payment > R10K AND < R30K): -";
                ViewBag.Header = "DETERMINE IRREGULAR EXPENDITURE FOR PAYMENTS GREATER THAN R 10 000 AND LESS THAN R 30 000";
                ViewBag.Payment = "R30K";
            }
            else if (totalAmount < 500000)
            {
                links[1] = "Payment R30K and R500K";
                ViewBag.Header = "Answer few questions here (Payment > R30K AND < R500K): -";
                ViewBag.Header = "DETERMINE IRREGULAR EXPENDITURE FOR PAYMENTS GREATER THAN R 30 000 AND LESS THAN R 499 000";
                ViewBag.Payment = "R500K";
            }
            else if (totalAmount >= 500000)
            {
                links[1] = "Payment Above R500K";
                ViewBag.Header = "Answer few questions here (Above R500K): -";
                ViewBag.Header = "DETERMINE IRREGULAR EXPENDITURE FOR PAYMENTS ABOVE R 500 000";
                ViewBag.Payment = "Above";
            }
            urls[1] = "Payments";

            TempData["LINK"] = links;
            TempData["URL"] = urls;
            ViewBag.links = links;
            ViewBag.urls = urls;

            return View(tempCase);
        }
        public ActionResult Phase2()
        {
            string UserID = User.Identity.GetUserId();

            CaseRepository caseRepo = new CaseRepository(context);
            Case tempCase = caseRepo.GetCaseTempByID(UserID);
            if (tempCase == null)
            {
                return RedirectToAction("Phase1", "Case");
            }
            if(!FixLinks(2))
            {
                return RedirectToAction("Phase1", "Case");
            }
            List<string> links = (List<string>)TempData["LINK"];
            List<string> urls = (List<string>)TempData["URL"];
            links.Add("Phase 2");
            urls.Add("Phase2");
            TempData["LINK"] = links;
            TempData["URL"] = urls;
            ViewBag.links = links;
            ViewBag.urls = urls;

            return View(tempCase);
        }

        public ActionResult Phase3()
        {
            string UserID = User.Identity.GetUserId();

            CaseRepository caseRepo = new CaseRepository(context);
            Case tempCase = caseRepo.GetCaseTempByID(UserID);
            if (tempCase == null)
            {
                return RedirectToAction("Phase1", "Case");
            }
            if (!FixLinks(3))
            {
                return RedirectToAction("Phase1", "Case");
            }
            List<string> links = (List<string>)TempData["LINK"];
            List<string> urls = (List<string>)TempData["URL"];
            links.Add("Phase 3");
            urls.Add("Phase3");
            TempData["LINK"] = links;
            TempData["URL"] = urls;
            ViewBag.links = links;
            ViewBag.urls = urls;

            return View(tempCase);
        }

        public ActionResult Phase41()
        {
            string UserID = User.Identity.GetUserId();

            CaseRepository caseRepo = new CaseRepository(context);
            Case tempCase = caseRepo.GetCaseTempByID(UserID);
            if (tempCase == null)
            {
                return RedirectToAction("Phase1", "Case");
            }
            if (!FixLinks(4))
            {
                return RedirectToAction("Phase1", "Case");
            }
            List<string> links = (List<string>)TempData["LINK"];
            List<string> urls = (List<string>)TempData["URL"];
            links.Add("Phase 4.1");
            urls.Add("Phase41");
            TempData["LINK"] = links;
            TempData["URL"] = urls;
            ViewBag.links = links;
            ViewBag.urls = urls;

            AnnexureF tempF;
            PaymentRepository repo = new PaymentRepository(new PWCContext());
            if (tempCase.AnnexureF.Count == 0)
            {
                tempF = new AnnexureF();
                tempCase.AnnexureF.Add(tempF);
            } else
            {
                tempF = tempCase.AnnexureF.FirstOrDefault();
            }
            tempF.F_DateOfFirstPayment = repo.GetDateOfFirstPayment(tempCase.CaseID);
            tempF.F_DateOfLastPayment = repo.GetDateOfLastPayment(tempCase.CaseID);
            tempF.F_Description = tempCase.Description;
            tempF.F_OfficialName = tempCase.OfficialName;
            tempF.F_Position = tempCase.Possition;
            tempF.F_Role = tempCase.Role;
            tempF.F_LossAmount = tempCase.CalculatedLoss;

            CultureInfo gb = new CultureInfo("en-GB");
            string result1 = tempF.F_DateOfFirstPayment?.ToString("dd/MM/yyyy", gb);
            string result2 = tempF.F_DateOfLastPayment?.ToString("dd/MM/yyyy", gb);
            ViewBag.FirstPayment = result1;
            ViewBag.LastPayment = result2;

            return View(tempCase);
        }

        public ActionResult Phase42()
        {
            string UserID = User.Identity.GetUserId();

            CaseRepository caseRepo = new CaseRepository(context);
            Case tempCase = caseRepo.GetCaseTempByID(UserID);
            if (tempCase == null)
            {
                return RedirectToAction("Phase1", "Case");
            }
            if (!FixLinks(5))
            {
                return RedirectToAction("Phase1", "Case");
            }
            List<string> links = (List<string>)TempData["LINK"];
            List<string> urls = (List<string>)TempData["URL"];
            links.Add("Phase 4.2");
            urls.Add("Phase42");
            TempData["LINK"] = links;
            TempData["URL"] = urls;
            ViewBag.links = links;
            ViewBag.urls = urls;

            return View(tempCase);
        }

        public ActionResult ShowIRRs()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var links = new List<string>
            {
                "Phase 1",
                "IRRs"
            };
            var urls = new List<string>
            {
                "Phase1",
                "ShowIRRs"
            };
            TempData["LINK"] = links;
            TempData["URL"] = urls;
            ViewBag.links = links;
            ViewBag.urls = urls;

            CaseRepository caseRepo = new CaseRepository(context);
            ViewBag.Cases = caseRepo.GetCases();
            return View();
        }

        public ActionResult Dashboard()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var links = new List<string>
            {
                "Phase 1",
                "DASHBOARD"
            };
            var urls = new List<string>
            {
                "Phase1",
                "Dashboard"
            };
            TempData["LINK"] = links;
            TempData["URL"] = urls;
            ViewBag.links = links;
            ViewBag.urls = urls;

            CaseRepository caseRepo = new CaseRepository(context);
            Summary summary = caseRepo.GetDashboard();
            return View(summary);
        }

        public ActionResult Report()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var links = new List<string>
            {
                "Phase 1",
                "REPORT"
            };
            var urls = new List<string>
            {
                "Phase1",
                "Report"
            };
            TempData["LINK"] = links;
            TempData["URL"] = urls;
            ViewBag.links = links;
            ViewBag.urls = urls;

            return View();
        }

        [HttpPost]
        public ActionResult Phase1Next(pwc.Models.Case caseModel)
        {
            string UserID = User.Identity.GetUserId();
            CaseRepository caseRepo = new CaseRepository(context);

            caseModel.UserId = UserID.ToString();
            caseRepo.SaveCase(caseModel);
            
            var tempCase = caseRepo.GetCaseTempByID(UserID);
            decimal totalAmount = caseRepo.TotalAmountOfPayments(tempCase.CaseID);
            if (totalAmount > 2000)
            {
                return RedirectToAction("Payments", "Case");
            }
            return RedirectToAction("UnsolicitedBids", "Case");
        }

        [HttpPost]
        public ActionResult UnsolicitedBidsNext(pwc.Models.Case caseModel)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            caseModel.UserId = Session["userID"].ToString();
            (new CaseRepository(context)).SaveCase(caseModel);
            return RedirectToAction("Phase2", "Case");
        }

        [HttpPost]
        public ActionResult PaymentsNext(pwc.Models.Case caseModel)
        {
            string UserID = User.Identity.GetUserId();

            var tempCase = (new CaseRepository(context)).GetCaseTempByID(UserID);
            SaveFile(caseModel.OneQuoteAttachedFile, "OneQuoteAttached", tempCase.CaseID);
            SaveFile(caseModel.ThreeQuotesAttachedFile, "ThreeQuotesAttached", tempCase.CaseID);
            SaveFile(caseModel.RelateToContractFile, "RelateToContract", tempCase.CaseID);
            SaveFile(caseModel.TransversalContractFile, "TransversalContract", tempCase.CaseID);
            SaveFile(caseModel.DeviationFile, "Deviation", tempCase.CaseID);
            SaveFile(caseModel.WinningSupplierFile, "WinningSupplier", tempCase.CaseID);

            caseModel.UserId = UserID.ToString();
            (new CaseRepository(context)).SaveCase(caseModel);
            return RedirectToAction("Phase2", "Case");
        }

        [HttpPost]
        public ActionResult Phase2Next(Case caseModel, AnnexureB BModel, AnnexureC CModel, AnnexureD DModel)
        {
            string UserID = User.Identity.GetUserId();

            var tempCase = (new CaseRepository(context)).GetCaseTempByID(UserID);
            SaveFile(caseModel.LowestQuoteFile, "LowestQuote", tempCase.CaseID);
            SaveFile(caseModel.ApprovedDeviationFile, "ApprovedDeviation", tempCase.CaseID);
            SaveFile(caseModel.AdequatelyDocumentedDeviationFile, "AdequatelyDocumentedDeviation", tempCase.CaseID);
            SaveFile(caseModel.GoodsWereRenderedFile, "GoodsWereRendered", tempCase.CaseID);
            SaveFile(caseModel.AssetRegisterFile, "AssetRegister", tempCase.CaseID);
            SaveFile(caseModel.DefinitionOfExpenditureFile, "DefinitionOfExpenditure", tempCase.CaseID);
            SaveFile(caseModel.RelatedPaymentsLossFile, "RelatedPaymentsLoss", tempCase.CaseID);
            SaveFile(caseModel.PaymentMadeFile, "PaymentMade", tempCase.CaseID);

            caseModel.UserId = UserID.ToString();
                        
            AnnexureBRepository BRepo = new AnnexureBRepository(context);
            AnnexureCRepository CRepo = new AnnexureCRepository(context);
            AnnexureDRepository DRepo = new AnnexureDRepository(context);
            
            BModel.CaseID = tempCase.CaseID;
            CModel.CaseID = tempCase.CaseID;
            DModel.CaseID = tempCase.CaseID;
            BRepo.Save(BModel);
            CRepo.Save(CModel);
            DRepo.Save(DModel);

            if (caseModel.Loss == "No")
            {
                caseModel.OfficialDetermining = "No";
                (new CaseRepository(context)).SaveCase(caseModel);
                return RedirectToAction("Phase41", "Case");
            }
            (new CaseRepository(context)).SaveCase(caseModel);
            return RedirectToAction("Phase3", "Case");
        }

        public void SaveFile(HttpPostedFileBase file, string fieldName, int caseID)
        {
            CaseFileRepository FRepo = new CaseFileRepository(context);
            try
            {
                if (file.ContentLength > 0)
                {
                    string _FieldName = fieldName;
                    string _FileNameOrigin = Path.GetFileName(file.FileName);
                    string _FileName = caseID + "_" + _FieldName + "_" + _FileNameOrigin;

                    string subPath = "~/UploadedFiles/" + caseID;
                    bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));
                    if (!exists)
                        System.IO.Directory.CreateDirectory(Server.MapPath(subPath));

                    string _path = Path.Combine(Server.MapPath(subPath), _FileName);
                    file.SaveAs(_path);

                    CaseFile tempFile = new CaseFile();
                    tempFile.CaseID = caseID;
                    tempFile.FieldName = _FieldName;
                    tempFile.FileName = _FileNameOrigin;
                    tempFile.FilePath = subPath + "/" + _FileName;
                    FRepo.SaveCaseFile(tempFile);
                }
            }
            catch
            {
            }
        }

        [HttpPost]
        public ActionResult Phase3Next(pwc.Models.Case caseModel)
        {
            string UserID = User.Identity.GetUserId();

            var tempCase = (new CaseRepository(context)).GetCaseTempByID(UserID);
            SaveFile(caseModel.OfficialDeterminingFile, "OfficialDetermining", tempCase.CaseID);
            SaveFile(caseModel.ExceedPowersFile, "ExceedPowers", tempCase.CaseID);
            SaveFile(caseModel.UseOfAlcoholFile, "UseOfAlcohol", tempCase.CaseID);
            SaveFile(caseModel.ActInScopeFile, "ActInScope", tempCase.CaseID);
            SaveFile(caseModel.ActRectlesslyFile, "ActRectlessly", tempCase.CaseID);
            SaveFile(caseModel.WithoutPriorConsultationFile, "WithoutPriorConsultation", tempCase.CaseID);
            SaveFile(caseModel.OfficialFailFile, "OfficialFail", tempCase.CaseID);

            caseModel.UserId = UserID.ToString();
            (new CaseRepository(context)).SaveCase(caseModel);
            return RedirectToAction("Phase41", "Case");
        }

        [HttpPost]
        public ActionResult Phase41Next(pwc.Models.Case caseModel, AnnexureF FModel)
        {
            string UserID = User.Identity.GetUserId();

            var tempCase = (new CaseRepository(context)).GetCaseTempByID(UserID);
            SaveFile(caseModel.MeetIrregularExpenditureFile, "MeetIrregularExpenditure", tempCase.CaseID);

            caseModel.UserId = UserID.ToString();

            AnnexureFRepository FRepo = new AnnexureFRepository(context);
            FModel.CaseID = tempCase.CaseID;
            FRepo.Save(FModel);

            (new CaseRepository(context)).SaveCase(caseModel);
            return RedirectToAction("Phase42", "Case");
        }

        [HttpPost]
        public ActionResult Phase42Next(pwc.Models.Case caseModel, AnnexureG GModel)
        {
            string UserID = User.Identity.GetUserId();

            caseModel.UserId = UserID;
            caseModel.Status = 1;

            AnnexureGRepository GRepo = new AnnexureGRepository(context);
            var tempCase = (new CaseRepository(context)).GetCaseTempByID(UserID);
            GModel.CaseID = tempCase.CaseID;
            GRepo.Save(GModel);

            CaseRepository repo = new CaseRepository(context);
            repo.SaveCase(caseModel);

            return RedirectToAction("Dashboard", "Case");
        }

        [HttpPost]
        public JsonResult PaymentAdd()
        {
            List<Payment> Payments = new List<Payment>();
            Payment tempPayment = new Payment();

            tempPayment.PaymentNumber = Request["inputPaymentNumber"];
            tempPayment.OrderNumber = Request["inputOrderNumber"];
            string date = Request["inputPaymentDate"];
            CultureInfo gb = new CultureInfo("en-GB");
            DateTime time = DateTime.Parse(date, gb);
            tempPayment.PaymentDate = time;
            tempPayment.PaymentAmount = Decimal.Parse(Request["inputPaymentAmount"]);

            string UserID = User.Identity.GetUserId();
            PaymentRepository repo = new PaymentRepository(context);
            var tempCase = (new CaseRepository(context)).GetCaseTempByID(UserID);
            tempPayment.CaseID = tempCase.CaseID;
            repo.SavePayment(tempPayment);

            var results = repo.GetPayments(tempCase.CaseID);
            foreach (var payment in tempCase.Payments)
            {
                Payment temp = new Payment();
                temp.PaymentID = payment.PaymentID;
                temp.PaymentNumber = payment.PaymentNumber;
                temp.OrderNumber = payment.OrderNumber;
                temp.PaymentDate = payment.PaymentDate;
                temp.PaymentAmount = payment.PaymentAmount;
                temp.DateString = payment.PaymentDate.ToString("dd/MM/yyyy", gb);
                Payments.Add(temp);
            }
            //Payments.Add(tempPayment);
            return Json(Payments);
        }

        [HttpGet]
        public JsonResult PaymentDelete(int id)
        {
            PaymentRepository repo = new PaymentRepository(context);
            repo.DeletePayment(id);
            return this.Json("success deleted!", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Upload(HttpPostedFileBase upload)
        {
            string UserID = User.Identity.GetUserId();
            PaymentRepository repo = new PaymentRepository(context);
            var tempCase = (new CaseRepository(context)).GetCaseTempByID(UserID);
            CultureInfo gb = new CultureInfo("en-GB");

            if (ModelState.IsValid)
            {

                if (upload != null && upload.ContentLength > 0)
                {

                    if (upload.FileName.EndsWith(".csv"))
                    {
                        Stream stream = upload.InputStream;
                        DataTable csvTable = new DataTable();
                        using (CsvReader csvReader =
                            new CsvReader(new StreamReader(stream), true))
                        {
                            csvTable.Load(csvReader);
                        }
                        //return View(csvTable);

                        foreach (DataRow row in csvTable.Rows)
                        {
                            Payment tempPayment = new Payment();
                            foreach (DataColumn col in csvTable.Columns)
                            {
                                switch (col.ColumnName)
                                {
                                    case "PaymentNumber":
                                        tempPayment.PaymentNumber = row[col.ColumnName].ToString();//row[col.ColumnName];
                                        break;
                                    case "OrderNumber":
                                        tempPayment.OrderNumber = row[col.ColumnName].ToString();
                                        break;
                                    case "PaymentDate":
                                        string date = row[col.ColumnName].ToString();
                                        DateTime time = DateTime.Parse(date, gb);
                                        tempPayment.PaymentDate = time;
                                        break;
                                    case "PaymentAmount":
                                        tempPayment.PaymentAmount = decimal.Parse(row[col.ColumnName].ToString());
                                        break;
                                }
                            }
                            tempPayment.CaseID = tempCase.CaseID;
                            repo.SavePayment(tempPayment);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        //return View();
                    }
                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }
            List<Payment> Payments = new List<Payment>();
            var results = repo.GetPayments(tempCase.CaseID);
            foreach (var payment in tempCase.Payments)
            {
                Payment temp = new Payment();
                temp.PaymentID = payment.PaymentID;
                temp.PaymentNumber = payment.PaymentNumber;
                temp.OrderNumber = payment.OrderNumber;
                temp.PaymentDate = payment.PaymentDate;
                temp.PaymentAmount = payment.PaymentAmount;
                temp.DateString = payment.PaymentDate.ToString("dd/MM/yyyy", gb);
                Payments.Add(temp);
            }
            //Payments.Add(tempPayment);
            return Json(Payments);
        }
    }
}