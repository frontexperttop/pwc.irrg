﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using pwc.Models;
using pwc.Repository;

namespace pwc.Controllers
{
    public class LoginController : Controller
    {
        
        private PWCContext context = new PWCContext();

        // GET: Login
        public ActionResult Index()
        {
            var links = new List<String>
            {
                "PwC Login",
            };
            var urls = new List<String>
            {
                "#",
            };
            ViewBag.links = links;
            ViewBag.urls = urls;
            return View();
        }

        [HttpPost]
        public ActionResult Authorize(pwc.Models.UserModel userModel)
        {
            var links = new List<String>
            {
                "Login",
            };
            ViewBag.links = links;
            
            //UserRepository userRepo = new UserRepository(context);
            //var UserID = userRepo.Authentication(userModel);
            //if (UserID == null)
            //{
            //    userModel.LoginErrorMessage = "Wrong username or password.";
            //    return View("Index", userModel);
            //} else
            //{
            //    Session["userID"] = UserID;
            //    return RedirectToAction("Phase1", "Case");
            //}
            return null;
        }
    }
}