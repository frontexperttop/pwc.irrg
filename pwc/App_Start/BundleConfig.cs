﻿using System.Web;
using System.Web.Optimization;

namespace pwc
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/vendor")                        
                        .Include("~/Scripts/jquery-{version}.js")
                        .Include("~/Scripts/jquery.validate*")
                        .Include("~/Scripts/modernizr-*")
                        .Include("~/Scripts/respond.js")
                        .Include("~/Scripts/moment.js")
                        .Include("~/Scripts/bootstrap-datetimepicker.js")
                        .Include("~/Scripts/select2.min.js"));


            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                      "~/Scripts/bootstrap/affix.js",
                      "~/Scripts/bootstrap/alert.js",
                      "~/Scripts/bootstrap/dropdown.js",
                      "~/Scripts/bootstrap/tooltip.js",
                      "~/Scripts/bootstrap/modal.js",
                      "~/Scripts/bootstrap/transition.js",
                      "~/Scripts/bootstrap/button.js",
                      "~/Scripts/bootstrap/popover.js",
                      "~/Scripts/bootstrap/carousel.js",
                      "~/Scripts/bootstrap/scrollspy.js",
                      "~/Scripts/bootstrap/collapse.js",
                      "~/Scripts/bootstrap/tab.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Scripts/site.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/css")                
                .Include("~/Content/_bootstrap.css")
                .Include("~/Content/styles/main.css")
                .Include("~/Content/css/select2.css")
                .Include("~/Content/bootstrap-datetimepicker.min.css"));
        }
    }
}
